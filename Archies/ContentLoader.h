//
//  ContentLoader.h
//  Archies
//
//  Created by Daniel Mendez on 6/24/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CategoryService.h"
#import "SubCategoryService.h"
#import <Realm/Realm.h>

#import "arCategory.h"
#import "arSubCategory.h"

@interface ContentLoader : NSObject{
    NSString *someProperty;
    NSString *baseUrl;
    CategoryService *categoryService;
    SubCategoryService *subCategoryService;
    NSMutableArray *loadingQueue;
    NSTimer *myTimer;
}

@property (nonatomic, copy) void (^readyCallback)(void);
@property (nonatomic, retain) NSString *someProperty;
@property (nonatomic, retain) NSString *baseUrl;
@property (nonatomic, retain) CategoryService *categoryService;
@property (nonatomic, retain) SubCategoryService *subCategoryService;
@property (nonatomic, retain) NSMutableArray *loadingQueue;
@property (nonatomic, retain) NSTimer *myTimer;

+ (id)sharedInstance;
- (void)getCategory;
- (void)getItemsByCategory:(arCategory *)category;
- (RLMResults *)getAllCategories;
- (RLMResults *)getDetail:(arCategory *)category;
- (void)removeFromQueue:(id)item;
@end