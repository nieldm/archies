//
//  ViewController.h
//  Archies
//
//  Created by Daniel Mendez on 6/24/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Category_DataSource.h"

@interface ViewController : UIViewController  <UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property Category_DataSource *dataSource;
@end

