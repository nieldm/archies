//
//  UpdateDBViewController.m
//  Archies
//
//  Created by Daniel Mendez on 6/26/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import "UpdateDBViewController.h"
#import "ContentLoader.h"
#import "ViewController.h"

@interface UpdateDBViewController ()

@end

@implementation UpdateDBViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //    [[NSFileManager defaultManager] removeItemAtPath:[RLMRealm defaultRealmPath] error:nil];
    ContentLoader *cl = [ContentLoader sharedInstance];
    [cl setReadyCallback:^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainNav"];
//        [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
        [UIView transitionWithView:self.view.window
                          duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{ self.view.window.rootViewController = rootViewController; }
                        completion:nil];
    }];
    [cl getCategory];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
