//
//  arCategory.h
//  Archies
//
//  Created by Daniel Mendez on 6/24/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import <Realm/Realm.h>
#import "arSubCategory.h"

@interface arCategory : RLMObject
@property int id;
@property NSString *name;
@property int type_id;
@property NSString *img_path;
@property int enabled;
@property (nonatomic, weak) NSDate *created_at;
@property (nonatomic, weak) NSDate *updated_at;

@property RLMArray<arSubCategory> *subCategories;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<arCategory>
RLM_ARRAY_TYPE(arCategory)
