//
//  SubCategoryService.h
//  Archies
//
//  Created by Daniel Mendez on 6/25/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubCategoryService : NSObject{
    NSString *baseUrl;
}

@property (nonatomic, retain) NSString *baseUrl;

-(void)getAllItems:(NSInteger)subCategoryId
        onSuccess:(void (^)(id responseObject))successCallback
        onError:(void (^)(NSError *errorObject))errorCallback;
@end