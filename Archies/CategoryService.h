//
//  CategoryService.h
//  Archies
//
//  Created by Daniel Mendez on 6/24/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryService : NSObject{
    NSString *baseUrl;
}

@property (nonatomic, retain) NSString *baseUrl;

-(void)getAll:(void (^)(id responseObject))successCallback
      onError:(void (^)(NSError *errorObject))errorCallback;

@end
