//
//  Category_Header_CollectionReusableView.h
//  Archies
//
//  Created by Daniel Mendez on 6/25/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Category_Header_CollectionReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UIImageView *catImage;
@property (weak, nonatomic) IBOutlet UILabel *catLabel;
@end
