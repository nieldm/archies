//
//  Category_DataSource.m
//  Archies
//
//  Created by Daniel Mendez on 6/25/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import "Category_DataSource.h"
#import "ContentLoader.h"
#import "SubCategory_CollectionViewCell.h"
#import "Category_Header_CollectionReusableView.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@implementation Category_DataSource

@synthesize content, readyCallback, myCollectionView;

-(void)loadCategories{
    ContentLoader *cl = [ContentLoader sharedInstance];
    self.content = [cl getAllCategories];
    readyCallback();
}

-(id)initWithReadyCallback:(void (^)(void))callback{
    if(self = [super init])
    {
        self.readyCallback = callback;
    }
    return self;
}

-(id)initWithReadyCallback:(void (^)(void))callback andCollectionView:(UICollectionView *)collectionView{
    if(self = [super init])
    {
        self.readyCallback = callback;
        
        self.myCollectionView = collectionView;
        
        [self.myCollectionView registerNib: [UINib nibWithNibName:@"SubCategory_CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"subCategoryBasic"];
        
        [self.myCollectionView registerNib:[UINib nibWithNibName:@"Category_Header_CollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"categoryHeaderBasic"];
        
        [self.myCollectionView setDataSource:self];
    }
    return self;
}

# pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.content count];
//    return [[[self.content objectAtIndex:section] valueForKey:@"subCategories"] count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SubCategory_CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"subCategoryBasic" forIndexPath:indexPath];
    arCategory *category = [self.content objectAtIndex:indexPath.row];
    cell.subCatLabel.text = [category.name capitalizedString];
    NSURL *url = [[NSURL alloc] initWithString:[@"http://192.237.180.31/archies/public/" stringByAppendingString:category.img_path]];
    [cell.subCatImage setImageWithURL:url];
    [cell.subCatImage.layer setCornerRadius:15.0];
    [cell.subCatImage.layer setMasksToBounds:YES];
    return cell;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    Category_Header_CollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"categoryHeaderBasic" forIndexPath:indexPath];
    view.catLabel.text = [@"Archies" uppercaseString];
    return view;
}
@end
