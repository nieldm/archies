//
//  CategoryDetailViewController.h
//  Archies
//
//  Created by Daniel Mendez on 6/25/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Category_Detail_DataSource.h"
#import "arCategory.h"

@interface CategoryDetailViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property arCategory *category;
@property Category_Detail_DataSource *dataSource;
@end
