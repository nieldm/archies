//
//  Category_DataSource.h
//  Archies
//
//  Created by Daniel Mendez on 6/25/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface Category_DataSource : NSObject <UICollectionViewDataSource>
@property UICollectionView *myCollectionView;
@property RLMResults *content;
@property (nonatomic, copy) void (^readyCallback)(void);

- (void)loadCategories;
- (id)initWithReadyCallback:(void (^)(void))callback;
- (id)initWithReadyCallback:(void (^)(void))callback
      andCollectionView:(UICollectionView *)collectionView;

@end
