//
//  arCategory.m
//  Archies
//
//  Created by Daniel Mendez on 6/24/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import "arCategory.h"

@implementation arCategory

+ (NSString *)primaryKey {
    return @"id";
}

// Specify default values for properties

+ (NSDictionary *)defaultPropertyValues
{
    return @{@"img_path" : @"",
             @"created_at": [NSDate date],
             @"updated_at": [NSDate date]};
}


// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

@end
