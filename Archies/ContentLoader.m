//
//  ContentLoader.m
//  Archies
//
//  Created by Daniel Mendez on 6/24/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import "ContentLoader.h"

@implementation ContentLoader

@synthesize someProperty, baseUrl, categoryService, subCategoryService, readyCallback, loadingQueue, myTimer;

#pragma mark Singleton Methods

+ (id)sharedInstance {
    static ContentLoader *sharedMyInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyInstance = [[self alloc] init];
    });
    return sharedMyInstance;
}

- (id)init {
    if (self = [super init]) {
        someProperty = @"Default Property Value";
        baseUrl = @"http://192.237.180.31/archies";
        categoryService = [[CategoryService alloc] init];
        [categoryService setBaseUrl:baseUrl];
        subCategoryService = [[SubCategoryService alloc] init];
        [subCategoryService setBaseUrl:baseUrl];
        loadingQueue = [[NSMutableArray alloc] init];
        readyCallback = nil;
    }
    return self;
}

-(void)removeFromQueue:(id)item{
    self.loadingQueue = (NSMutableArray *) [self.loadingQueue filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(id != %@)", [item valueForKey:@"id"]]];
    if ([self.loadingQueue count] == 0) {
        if ([myTimer isValid])
        {
            [myTimer invalidate];
        }
        myTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                   target:self
                                                 selector:@selector(didFinishLoading)
                                                 userInfo:nil
                                                  repeats:YES];
    }
}

-(void)didFinishLoading{
    if ([self.loadingQueue count] == 0) {
        if(readyCallback != nil){
            readyCallback();
        }
        [self.myTimer invalidate];
    }else{
    }
}

-(void)getCategory{
    NSLog(@"[!] Categories Start Loading");
    [self.categoryService getAll:^(id responseObject) {
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        NSLog(@"[+] Categories Loaded!");
        self.loadingQueue = (NSMutableArray *)[self.loadingQueue arrayByAddingObjectsFromArray:responseObject];
        for (NSDictionary *category in responseObject) {
            arCategory *arCat = [[arCategory alloc] init];
            arCat.id = [[category valueForKey:@"id"] intValue];
            arCat.name = [category valueForKey:@"name"];
            NSString *img_path = [category valueForKey:@"img_path"];
            if ((NSString *)[NSNull null] != img_path) {
                arCat.img_path = img_path;
            }
            arCat.type_id = [[category valueForKey:@"id"] intValue];
            arCat.enabled = [[category valueForKey:@"enabled"] intValue];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *created_at = [dateFormatter dateFromString: [category valueForKey:@"created_at"]];
            NSDate *updated_at = [dateFormatter dateFromString: [category valueForKey:@"updated_at"]];
            
            if (created_at != nil) {
                arCat.created_at = created_at;
            }
            if (updated_at != nil) {
                arCat.updated_at = updated_at;
            }
            [self getItemsByCategory:arCat];

            
            [arCategory createOrUpdateInDefaultRealmWithValue:arCat];
            [self removeFromQueue:category];
        }
        [realm commitWriteTransaction];
        NSLog(@"[+] Categories Saved!");
    } onError:^(NSError *errorObject) {
        NSLog(@"[x] Categories Error Loading");
    }];
}

- (void)getItemsByCategory:(arCategory *)category{
    [self.subCategoryService getAllItems:category.id onSuccess:^(id responseObject) {
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        NSLog(@"[!] SubCategories Start Loading for Category %ld", (long) category.id);
        self.loadingQueue = (NSMutableArray *)[self.loadingQueue arrayByAddingObjectsFromArray:[responseObject objectForKey:@"subcategory"]];
        for (NSDictionary *subCategory in [responseObject objectForKey:@"subcategory"]) {
            arSubCategory *arSubCat = [[arSubCategory alloc] init];
            arSubCat.id = [[subCategory valueForKey:@"id"] intValue];
            arSubCat.name = [subCategory valueForKey:@"name"];
            arSubCat.category_id = [[subCategory valueForKey:@"category_id"] intValue];
            arSubCat.addition_enable = [[subCategory valueForKey:@"addition_enable"] intValue];
            arSubCat.enabled = [[subCategory valueForKey:@"enabled"] intValue];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *created_at = [dateFormatter dateFromString: [subCategory valueForKey:@"created_at"]];
            NSDate *updated_at = [dateFormatter dateFromString: [subCategory valueForKey:@"updated_at"]];
            
            if (created_at != nil) {
                arSubCat.created_at = created_at;
            }
            if (updated_at != nil) {
                arSubCat.updated_at = updated_at;
            }
            [category.subCategories addObject:arSubCat];
            self.loadingQueue = (NSMutableArray *)[self.loadingQueue arrayByAddingObjectsFromArray:[subCategory objectForKey:@"items"]];
            for (NSDictionary *item in [subCategory objectForKey:@"items"]) {
                NSLog(@"[!] Items Start Loading for Category %ld SubCategory %ld", (long) category.id, (long) arSubCat.id);
                arItem *arIt = [[arItem alloc] init];
                arIt.id = [[item valueForKey:@"id"] intValue];
                arIt.name = [item valueForKey:@"name"];
                
                NSString *value = [item valueForKey:@"description"];
                if ((NSString *)[NSNull null] != value) {
                    arIt.item_description = value;
                }
                value = [item valueForKey:@"img_path"];
                if ((NSString *)[NSNull null] != value) {
                    arIt.img_path = value;
                }
                value = [item valueForKey:@"left_img_path"];
                if ((NSString *)[NSNull null] != value) {
                    arIt.left_img_path = value;
                }
                value = [item valueForKey:@"right_img_path"];
                if ((NSString *)[NSNull null] != value) {
                    arIt.right_img_path = value;
                }
                
                arIt.subcategory_id = [[item valueForKey:@"subcategory_id"] intValue];
                arIt.enabled = [[item valueForKey:@"enabled"] intValue];
                
                NSDate *created_at = [dateFormatter dateFromString: [item valueForKey:@"created_at"]];
                NSDate *updated_at = [dateFormatter dateFromString: [item valueForKey:@"updated_at"]];
                
                if (created_at != nil) {
                    arIt.created_at = created_at;
                }
                if (updated_at != nil) {
                    arIt.updated_at = updated_at;
                }
                
                [arSubCat.items addObject:arIt];
                [self removeFromQueue:item];
            }
            [self removeFromQueue:subCategory];
        }
        [arCategory createOrUpdateInDefaultRealmWithValue:category];
        [realm commitWriteTransaction];
    } onError:^(NSError *errorObject) {
        NSLog(@"[x] SubCategories Error Loading");
        NSLog(@"[x] Items Error Loading");
        NSLog(@"Error %@", errorObject);
    }];
}

-(RLMResults *)getAllCategories{
    RLMResults *enabledCategories = [[arCategory objectsWhere:@" enabled == 1 AND ANY subCategories.enabled == 1"] sortedResultsUsingProperty:@"id" ascending:YES];
    return enabledCategories;
}

-(RLMResults *)getDetail:(arCategory *)category{
    RLMResults *enabledCategories = [category.subCategories objectsWhere:@" enabled == 1 AND ANY items.enabled == 1"];
    return enabledCategories;
}

@end