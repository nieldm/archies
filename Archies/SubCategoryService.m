//
//  SubCategoryService.m
//  Archies
//
//  Created by Daniel Mendez on 6/25/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import "SubCategoryService.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@implementation SubCategoryService

@synthesize baseUrl = _baseUrl;

-(void)getAllItems:(NSInteger)subCategoryId onSuccess:(void (^)(id))successCallback onError:(void (^)(NSError *))errorCallback{
    NSString *paramsUrl = [@"" stringByAppendingFormat:@"/public/category/details/%ld", (long)subCategoryId];
    NSString *url = [_baseUrl stringByAppendingString:paramsUrl];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        successCallback(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorCallback(error);
    }];
}
@end
