//
//  CategoryService.m
//  Archies
//
//  Created by Daniel Mendez on 6/24/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import "CategoryService.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@implementation CategoryService

@synthesize baseUrl = _baseUrl;

-(void)getAll:(void (^)(id))successCallback onError:(void (^)(NSError *))errorCallback{
    NSString *url = [_baseUrl stringByAppendingString:@"/public/category"];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        successCallback(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorCallback(error);
    }];
}

@end
