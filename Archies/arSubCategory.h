//
//  arSubCategory.h
//  Archies
//
//  Created by Daniel Mendez on 6/24/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import <Realm/Realm.h>
#import "arItem.h"

@interface arSubCategory : RLMObject
@property NSInteger id;
@property NSString *name;
@property NSInteger category_id;
@property int enabled;
@property int addition_enable;
@property NSDate *created_at;
@property NSDate *updated_at;

@property RLMArray<arItem> *items;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<arSubCategory>
RLM_ARRAY_TYPE(arSubCategory)
