//
//  Category_Detail_DataSource.m
//  Archies
//
//  Created by Daniel Mendez on 6/25/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import "Category_Detail_DataSource.h"
#import "ContentLoader.h"
#import "Item_CollectionViewCell.h"
#import "Category_Header_CollectionReusableView.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@implementation Category_Detail_DataSource

@synthesize content, readyCallback, myCollectionView;

-(void)loadCategories{
    ContentLoader *cl = [ContentLoader sharedInstance];
    self.content = [cl getAllCategories];
    readyCallback();
}

-(void)loadDetail:(arCategory *)category{
    ContentLoader *cl = [ContentLoader sharedInstance];
    self.content = [cl getDetail:category];
    readyCallback();
}

-(id)initWithReadyCallback:(void (^)(void))callback{
    if(self = [super init])
    {
        self.readyCallback = callback;
    }
    return self;
}

-(id)initWithReadyCallback:(void (^)(void))callback andCollectionView:(UICollectionView *)collectionView{
    if(self = [super init])
    {
        self.readyCallback = callback;
        
        self.myCollectionView = collectionView;
        
        [self.myCollectionView registerNib: [UINib nibWithNibName:@"Item_CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"itemBasic"];
        
        [self.myCollectionView registerNib:[UINib nibWithNibName:@"Category_Header_CollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"categoryHeaderBasic"];
        
        [self.myCollectionView setDataSource:self];
    }
    return self;
}

# pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return [self.content count];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [[[self.content objectAtIndex:section] valueForKey:@"items"] count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    Item_CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"itemBasic" forIndexPath:indexPath];
    arItem *item = [[[self.content objectAtIndex:indexPath.section] valueForKey:@"items"] objectAtIndex:indexPath.row];
    cell.itemName.text = [item.name capitalizedString];
    cell.itemDescription.text = item.item_description;
    NSURL *url = [[NSURL alloc] initWithString:[@"http://192.237.180.31/archies/public/" stringByAppendingString:item.img_path]];
    [cell.itemImage setImageWithURL:url];
    [cell.itemImage.layer setCornerRadius:15.0];
    [cell.itemImage.layer setMasksToBounds:YES];
    return cell;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    Category_Header_CollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"categoryHeaderBasic" forIndexPath:indexPath];
    arSubCategory *subCat = [self.content objectAtIndex:indexPath.section];
    view.catLabel.text = [subCat.name uppercaseString];
    return view;
}
@end
