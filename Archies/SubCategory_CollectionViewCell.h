//
//  SubCategory_CollectionViewCell.h
//  Archies
//
//  Created by Daniel Mendez on 6/25/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubCategory_CollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *subCatLabel;
@property (weak, nonatomic) IBOutlet UIImageView *subCatImage;
@end
