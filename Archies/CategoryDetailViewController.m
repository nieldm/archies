//
//  CategoryDetailViewController.m
//  Archies
//
//  Created by Daniel Mendez on 6/25/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import "CategoryDetailViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@interface CategoryDetailViewController ()
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *catImage;
@property (weak, nonatomic) IBOutlet UILabel *catLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation CategoryDetailViewController

@synthesize category;

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    self.dataSource = [[Category_Detail_DataSource alloc] initWithReadyCallback:^{
        NSLog(@"Contents: %@", self.dataSource.content);
        [self.collectionView reloadData];
    } andCollectionView:self.collectionView];
    [self.collectionView setDelegate:self];
    [self.collectionView.layer setCornerRadius:15.0];
    [self.dataSource loadDetail:self.category];
    
    self.catLabel.text = [self.category.name uppercaseString];
    NSURL *url = [[NSURL alloc] initWithString:[@"http://192.237.180.31/archies/public/" stringByAppendingString:category.img_path]];
    [self.catImage setImageWithURL:url];
    [self.catImage.layer setCornerRadius:15.0];
    [self.catImage.layer setMasksToBounds:YES];
    
    [self.collectionView.layer setCornerRadius:15.0];
    [self.collectionView.layer setMasksToBounds:YES];
}
- (IBAction)dismissView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDelegate UICollectionViewDelegateFlowLayout
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(self.view.frame.size.width - 40, 88.0);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.collectionView.frame.size.width/2 - 20, 100.0);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsZero;
        return UIEdgeInsetsMake(10.0, 5.0, 10.0, 5.0);
}

@end
