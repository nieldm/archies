//
//  arItem.h
//  Archies
//
//  Created by Daniel Mendez on 6/24/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import <Realm/Realm.h>

@interface arItem : RLMObject
@property NSInteger id;
@property NSString *name;
@property NSString *item_description;
@property NSString *img_path;
@property NSString *left_img_path;
@property NSString *right_img_path;
@property NSInteger subcategory_id;
@property int enabled;
@property NSDate *created_at;
@property NSDate *updated_at;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<arItem>
RLM_ARRAY_TYPE(arItem)
