//
//  main.m
//  Archies
//
//  Created by Daniel Mendez on 6/24/15.
//  Copyright (c) 2015 Daniel Mendez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
